package com.example.fitnessapp2

import android.content.Intent
import android.os.Bundle
import android.view.*
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.fitnessapp2.db.LocalDatabaseHandler
import com.google.android.material.tabs.TabLayout

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

   // lateinit var button_save: Button

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    var localDatabaseHandler: LocalDatabaseHandler? = null
    lateinit var einheitenFragment: TabLayout.Tab

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        viewPager = findViewById<ViewPager>(R.id.viewPager)
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Einheiten"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Vorlagen"))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MainTabAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
                setFabBehavior(tab.position)
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        setFabBehavior(tabLayout!!.selectedTabPosition)
    }




    private fun setFabBehavior(tab_position: Int) {
        if (tab_position == 0) {
            fab.setOnClickListener {
                val intent = Intent(this, AddEinheit::class.java)
                startActivity(intent)
            }
        } else if (tab_position == 1) {
            fab.setOnClickListener {view ->
                Snackbar.make(view, "vorlagen", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


}
