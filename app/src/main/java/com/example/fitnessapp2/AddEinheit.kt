package com.example.fitnessapp2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import com.example.fitnessapp2.db.LocalDatabaseHandler
import com.example.fitnessapp2.model.EinheitDbEntry
import android.widget.*
import org.json.JSONObject
import java.util.*



class AddEinheit : AppCompatActivity() {

    var localDatabaseHandler: LocalDatabaseHandler? = null
    lateinit var datePicker : DatePicker
    var editTextS : MutableList<EditText> = mutableListOf()
    lateinit var fieldLayout : LinearLayout
    lateinit var vorlagenAuswahl : Spinner


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_einheit)


        localDatabaseHandler = LocalDatabaseHandler(this)

        // TODO get vorlagen from db
        var vorlagen = mutableListOf("Vorlage1", "Vorlage2", "Vorlage3", "...")
        vorlagenAuswahl = findViewById(R.id.vorlagen_auswahl)
        if (vorlagenAuswahl != null) {
            val adapter = ArrayAdapter(
                this,
                R.layout.view_spinner_item, vorlagen
            )
            vorlagenAuswahl.adapter = adapter
        }

        datePicker = findViewById(R.id.date_Picker)
        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)
        datePicker.init(year, month, day, null)

        // TODO fields must be changed according to selected vorlage
        var vorlagenName = "Laufen"
        var fields = mutableListOf("Distanz", "Dauer")
        fieldLayout = findViewById(R.id.field_layout)
        fields.forEach{
            var tempEditText = EditText(this)
            tempEditText.hint=it
            tempEditText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20.toFloat())
            tempEditText.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            fieldLayout.addView(tempEditText)
            editTextS.add(tempEditText)
        }

        //val editTextDistance = findViewById<EditText>(R.id.editText_distance)
        val buttonSave = findViewById<Button>(R.id.button_save)
        buttonSave.setOnClickListener {
            val einheitDbEntry = EinheitDbEntry()
            var success: Boolean = false
            var json = JSONObject()
            json.put("Vorlage", vorlagenName)
            editTextS.forEach{
                json.put(it.hint.toString(), it.text.toString())
            }
            einheitDbEntry.json = json.toString()

            var calender = Calendar.getInstance()
            calender.set(datePicker.year, datePicker.month, datePicker.dayOfMonth)
            einheitDbEntry.date = Date(calender.timeInMillis)

            success = localDatabaseHandler!!.addEinheit(einheitDbEntry)

            if (success){
                val toast = Toast.makeText(this,"Saved Successfully", Toast.LENGTH_LONG).show()

                finish()

            } else {
                val toast = Toast.makeText(this,"Please enter numbers only!", Toast.LENGTH_LONG).show()
            }

            // checking input text should not be null
            /*if (editTextDistance.text.toString() != ""){
                val dbEntry: EinheitDbEntry = EinheitDbEntry()
                var success: Boolean = false
                dbEntry.json = editTextDistance.text.toString()

                success = localDatabaseHandler!!.addEinheit(dbEntry)

                if (success){
                    val toast = Toast.makeText(this,"Saved Successfully", Toast.LENGTH_LONG).show()

                    //val einheitenFragment = supportFragmentManager.findFragmentByTag("einheiten")
                    finish()
                }
            } else {
                val toast = Toast.makeText(this,"Please enter numbers only!", Toast.LENGTH_LONG).show()
            }*/
        }





    }
}
