package com.example.fitnessapp2

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class EinheitenListAdapter(private var myDataset: MutableList<Pair<Date, String>>) :
    RecyclerView.Adapter<EinheitenViewHolder>() {


    fun update(myNewDataset: MutableList<Pair<Date, String>>){
        myDataset = myNewDataset
        notifyDataSetChanged()
    }




    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): EinheitenViewHolder {
        // create a new view


        val inflater = LayoutInflater.from(parent.context)
        return EinheitenViewHolder(inflater, parent)
    }

    // Replace the contents of a view (invoked by the layout manager)

    override fun onBindViewHolder(holder: EinheitenViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.bind(myDataset[position])
    }



    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size


}
