package com.example.fitnessapp2

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class EinheitenViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.view_einheiten_item, parent, false)) {
    private var dateTextView: TextView? = null
    private var textTextView: TextView? = null


    init {
        dateTextView = itemView.findViewById(R.id.einheiten_date)
        textTextView = itemView.findViewById(R.id.einheiten_text)
    }

    fun bind(pair: Pair<Date, String>) {
        val c = Calendar.getInstance()
        c.timeInMillis = pair.first.time
        val sdf = SimpleDateFormat("dd.MM.\nyyyy")
        dateTextView?.text = sdf.format(c.time)

        val jsonString = pair.second
        val json = JSONObject(jsonString)
        val iter = json.keys()
        var text = ""
        while (iter.hasNext()) {
            val key = iter.next()
            val value = json.get(key)
            text += key + ": " + value
            if (iter.hasNext()) text += "\n"
        }
        textTextView?.text = text
    }

}

