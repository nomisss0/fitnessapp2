package com.example.fitnessapp2.db

import android.database.sqlite.SQLiteOpenHelper
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.example.fitnessapp2.model.EinheitDbEntry
import java.util.*

class LocalDatabaseHandler(context: Context) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        var CREATE_TABLE = "CREATE TABLE $EINHEITEN_TABLE_NAME " +
                "($ID Integer PRIMARY KEY, $JSON TEXT, $DATE DATE)"
        Log.v("tablecreate", db?.execSQL(CREATE_TABLE).toString())
        CREATE_TABLE = "CREATE TABLE $VORLAGEN_TABLE_NAME " +
                "($ID Integer PRIMARY KEY, $JSON TEXT)"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + EINHEITEN_TABLE_NAME)
        db?.execSQL("DROP TABLE IF EXISTS " + VORLAGEN_TABLE_NAME)
        onCreate(db)
    }

    /* Old way, not used anymore, because of detailed EinheitDbEntry Class - may be reused later, if VorlagenDbEntry and EinheitenDbEntry get a super class.

    fun addEntry(table: String, dbEntry: DbEntry): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(JSON, dbEntry.json)
        values.put(DATE, dbEntry.date.time)
        val _success = db.insert(table, null, values)
        db.close()
        Log.v("InsertedID", "$_success")
        return (Integer.parseInt("$_success") != -1)
    }

    fun addVorlagedbEntry(dbEntry: DbEntry): Boolean {
         return addEntry("$VORLAGEN_TABLE_NAME", einheitDbEntry)
    }

     */

    private fun addEntry(table: String, values: ContentValues) : Boolean {
        val db = this.writableDatabase
        val _success = db.insert(table, null, values)
        db.close()
        return (Integer.parseInt("$_success") != -1)
    }


    fun addEinheit(einheitDbEntry: EinheitDbEntry): Boolean{
        val values = ContentValues()
        values.put(JSON, einheitDbEntry.json)
        values.put(DATE, einheitDbEntry.date.time)
        return addEntry(EINHEITEN_TABLE_NAME, values)
    }

    /* old, replaced by getAllEinheitenList, may be reused later?
    fun getAllEinheiten(): String {
        var allRuns = ""
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $EINHEITEN_TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        Log.d("test", cursor.toString())
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    var id = cursor.getString(cursor.getColumnIndex(ID))
                    var json = cursor.getString(cursor.getColumnIndex(JSON))


                    allRuns = "$allRuns\n$id $json "
                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allRuns
    }
     */

    /* old, replaced by getAllEinheitenListPairs, may be reused later?
    fun getAllEinheitenList(): MutableMap<Int, String> {
        var allEinheiten = mutableMapOf<Int, String>()
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $EINHEITEN_TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        var c = Calendar.getInstance()
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val date = Date(cursor.getString(cursor.getColumnIndex(DATE)).toLong())
                    c.timeInMillis = date.time
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    var temp = sdf.format(c.time)
                    temp += " - " + cursor.getString(cursor.getColumnIndex(JSON))
                    allEinheiten.put(cursor.getInt(cursor.getColumnIndex(ID)),temp)
                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allEinheiten
    }
     */

    fun getAllEinheitenListPairs(): MutableMap<Int, Pair<Date, String>> {
        var allEinheiten = mutableMapOf<Int, Pair<Date, String>>()
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $EINHEITEN_TABLE_NAME"
        val cursor = db.rawQuery(selectALLQuery, null)
        var c = Calendar.getInstance()
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val date = Date(cursor.getString(cursor.getColumnIndex(DATE)).toLong())
                    val json = cursor.getString(cursor.getColumnIndex(JSON))
                    val temp = Pair(date, json)
                    allEinheiten.put(cursor.getInt(cursor.getColumnIndex(ID)),temp)
                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allEinheiten
    }

    fun getAllEinheiten(): MutableList<EinheitDbEntry> {
        var allEinheiten = mutableListOf<EinheitDbEntry>()
        val cursor = getAllEntries(EINHEITEN_TABLE_NAME)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    var einheitDbEntry = EinheitDbEntry()
                    einheitDbEntry.date = Date(cursor.getString(cursor.getColumnIndex(DATE)).toLong())
                    einheitDbEntry.json = cursor.getString(cursor.getColumnIndex(JSON))
                    einheitDbEntry.id = cursor.getInt(cursor.getColumnIndex(ID))
                    allEinheiten.add(einheitDbEntry)
                } while (cursor.moveToNext())
            }
        }
        return allEinheiten
    }

    fun getAllEntries(table: String) : Cursor {
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $table"
        return db.rawQuery(selectALLQuery, null)
    }

    companion object {
        private val DB_NAME = "localDB"
        private val DB_VERSION = 6
        private val EINHEITEN_TABLE_NAME = "einheiten"
        private val VORLAGEN_TABLE_NAME = "vorlagen"
        private val ID = "id"
        private val DATE = "date"
        private val JSON = "json"
    }
}