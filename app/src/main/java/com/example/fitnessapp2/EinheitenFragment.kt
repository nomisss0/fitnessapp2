package com.example.fitnessapp2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fitnessapp2.db.LocalDatabaseHandler
import java.util.*
import kotlin.collections.ArrayList

class EinheitenFragment : Fragment() {


    lateinit var button_show: Button
    private lateinit var recyclerView: RecyclerView
    lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    var localDatabaseHandler: LocalDatabaseHandler? = null

    fun update(){
        val myDataset = localDatabaseHandler!!.getAllEinheitenListPairs()
        val myDatasetSorted = myDataset.toSortedMap(
            compareByDescending { it }
        )
        (viewAdapter as EinheitenListAdapter).update(ArrayList<Pair<Date, String>>(myDatasetSorted.values))
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = activity as Context
        localDatabaseHandler = LocalDatabaseHandler(activity)

        viewManager = LinearLayoutManager(context)

        val myDataset = localDatabaseHandler!!.getAllEinheitenListPairs()
        val myDatasetSorted = myDataset.toSortedMap(
            compareByDescending { it }
        )
        viewAdapter = EinheitenListAdapter(ArrayList<Pair<Date, String>>(myDatasetSorted.values))

        recyclerView = view!!.findViewById<RecyclerView>(R.id.einheiten_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater!!.inflate(R.layout.fragment_einheiten, container, false)

        return view
    }

    override fun onResume() {
        super.onResume()
        update()
    }
}



